package eu.telecomnancy;

import eu.telecomnancy.sensor.Adapter;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class App {
	
	// Code pour l'Adaptateur
    public static void main(String[] args) {
        LegacyTemperatureSensor LTS = new LegacyTemperatureSensor();
        ISensor old_sensor = new Adapter(LTS);
        new ConsoleUI(old_sensor);
    }
}