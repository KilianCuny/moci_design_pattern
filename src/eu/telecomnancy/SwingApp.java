package eu.telecomnancy;

import eu.telecomnancy.sensor.Decorator_Arrondis;
import eu.telecomnancy.sensor.Decorator_Fahrenheit;
import eu.telecomnancy.sensor.Fabrique;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.Proxy;
import eu.telecomnancy.sensor.StateTemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.sensor.TypeUnite;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

	// Code Observateur
    public static void main(String[] args) {
    	
    	// Code Observateur
        ISensor sensor = new TemperatureSensor();
        //new MainWindow(sensor, false, false);
        
        // Code Etat
        ISensor sensor_state = new StateTemperatureSensor();
        //new MainWindow(sensor_state, false, false);
        
        // Code Proxy
        Proxy p = new Proxy(sensor);
        //new MainWindow(p, false, false);
        
        // Code Decorateur
        ISensor sensor_arrondis = new Decorator_Arrondis();
        //new MainWindow(sensor_arrondis, true, true);
        
        ISensor sensor_fahre = new Decorator_Fahrenheit();
        //new MainWindow(sensor_fahre, true, false);
        
        // Code Fabrique
        Fabrique f = new Fabrique();
        // More type on TypeUnite.java
        ISensor fab = f.creerCapteur(TypeUnite.TemperatureSensor);
        new MainWindow(fab, false, false);
    }
}
