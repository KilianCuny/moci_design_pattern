package eu.telecomnancy.sensor;

public class Decorator_Fahrenheit extends TemperatureSensor {

	boolean celsius = true;
	
	public Decorator_Fahrenheit(){
		super();
	}
	
	public void convert(double temp){
		if (celsius){
			this.value = 1.08*temp + 32;
			celsius = false;
		}else{
			this.value = (temp - 32)/1.08;
			celsius = true;
		}
		setChanged();
		notifyObservers();
	}
}
