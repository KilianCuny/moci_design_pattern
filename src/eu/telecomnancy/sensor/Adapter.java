/**
 * @author Kilian Cuny
 * Adapte LegacyTemperatureSensor � l'interface ISensor
 */

package eu.telecomnancy.sensor;

import java.util.Observable;

public class Adapter extends Observable implements ISensor {

	double last_temp; // Derni�re temp�rature enregistr�e
	private LegacyTemperatureSensor LTS;
	
	public Adapter(LegacyTemperatureSensor LTS){
		this.LTS = LTS;
		this.last_temp = 0;
	}
	
	public void on() {
		if(!LTS.getStatus())
			LTS.onOff();
	}

	public void off() {
		if(LTS.getStatus())
			LTS.onOff();
	}

	public boolean getStatus() {
		return LTS.getStatus();
	}

	public void update() throws SensorNotActivatedException {
		// In order to get the same state before and after
		if(getStatus())
			LTS.onOff();
		else throw new SensorNotActivatedException("Sensor must be activated to update its value.");
		
		LTS.onOff();
		last_temp = LTS.getTemperature();
		this.setChanged();
		this.notifyObservers();
	}

	public double getValue() throws SensorNotActivatedException {
		if(LTS.getStatus())
			return last_temp;
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}	
}