/**
 * @author Kilian Cuny
 * Etat allum� du capteur
 */


package eu.telecomnancy.sensor;

import java.util.Random;

public class SensorOn extends StateTemperatureSensor{
	protected double value;

	public SensorOn(){
		value = 0;
	}
	
	public boolean getStatus(){
		return true;
	}
	
	public void update(){
		value = (new Random()).nextDouble() * 100;
	}
	
	public double getValue() throws SensorNotActivatedException{
		return value;
	}
}
