package eu.telecomnancy.sensor;

import java.util.Date;
import java.util.Observable;

public class Proxy extends Observable implements ISensor {
	
	ISensor i;
	
	public Proxy(ISensor i){
		this.i = i;
	}
		
	public void on() {
		System.out.println("[on] : " + new Date());
		i.on();
	}

	public void off() {
		System.out.println("[off] : " + new Date());
		i.off();
	}

	public boolean getStatus() {
		System.out.println("[getStatus] : " + new Date() + " : "+i.getStatus());
		return i.getStatus();
	}

	public void update() throws SensorNotActivatedException {
		System.out.println("[update] : " + new Date());
		i.update();
		this.setChanged();
		this.notifyObservers();
	}

	public double getValue() throws SensorNotActivatedException {
		System.out.println("[getValue] : " + new Date() + " : "+i.getValue());
		return i.getValue();
	}	
}
