package eu.telecomnancy.sensor;

public class Decorator_Arrondis extends TemperatureSensor {

	public Decorator_Arrondis(){
		super();
	}
	
	public void round(double temp){
		this.value = Math.round(temp);
		setChanged();
		notifyObservers();
	}
}
