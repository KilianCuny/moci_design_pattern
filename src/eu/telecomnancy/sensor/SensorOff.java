/**
 * @author Kilian Cuny
 * Etat eteint du capteur
 */

package eu.telecomnancy.sensor;

public class SensorOff extends StateTemperatureSensor{
	protected double value;
	
	public SensorOff(){
		value = 0;
	}
	
	public boolean getStatus(){
		return false;
	}
	
	public void update() throws SensorNotActivatedException{
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}
	
	public double getValue() throws SensorNotActivatedException{
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
}
