/**
 * @author Kilian Cuny
 * Create an instance of ISensor depending on the type asked
 */

package eu.telecomnancy.sensor;

public class Fabrique {
	
	public Fabrique(){
		
	}

	public ISensor creerCapteur(TypeUnite type) {
		ISensor capteur = null;

		switch (type) {
		case TemperatureSensor:
			capteur = new TemperatureSensor();
			break;
		case Decorator_Arrondis:
			capteur = new Decorator_Arrondis();
			break;
		case Decorator_Fahrenheit:
			capteur = new Decorator_Fahrenheit();
			break;
		case StateTemperatureSensor:
			capteur = new StateTemperatureSensor();
			break;
		}
		return capteur;
	}
}
