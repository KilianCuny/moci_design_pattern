/**
 * @author Kilian Cuny
 * R�alise le design pattern Etat
 */

package eu.telecomnancy.sensor;

import java.util.Observable;

public class StateTemperatureSensor extends Observable implements ISensor {
	private StateTemperatureSensor state;

    public void on() {
        state = new SensorOn();
    }

    public void off() {
        state = new SensorOff();
    }

    public boolean getStatus() {
        return state.getStatus();

    }

    public void update() throws SensorNotActivatedException {
        state.update();
        this.setChanged();
		this.notifyObservers();
    }

    public double getValue() throws SensorNotActivatedException {
        return state.getValue();
    }

}
