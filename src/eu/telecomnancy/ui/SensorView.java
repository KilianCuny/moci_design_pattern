package eu.telecomnancy.ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import eu.telecomnancy.sensor.Decorator_Arrondis;
import eu.telecomnancy.sensor.Decorator_Fahrenheit;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

@SuppressWarnings("serial")
public class SensorView extends JPanel implements Observer {
	private ISensor sensor;

	private JLabel value = new JLabel("N/A �C");
	private JButton on = new JButton("On");
	private JButton off = new JButton("Off");
	private JButton vide = new JButton("");
	private JButton update = new JButton("Update");
	private JButton round = new JButton("Round");
	private JButton fahrenheit = new JButton("C�/F�");

	public SensorView(ISensor c, boolean actif, boolean arrondis) {
		this.sensor = c;
		this.setLayout(new BorderLayout());

		value.setHorizontalAlignment(SwingConstants.CENTER);
		Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
		value.setFont(sensorValueFont);

		this.add(value, BorderLayout.CENTER);

		on.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sensor.on();
			}
		});

		off.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sensor.off();
			}
		});

		update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					sensor.update();
				} catch (SensorNotActivatedException sensorNotActivatedException) {
					sensorNotActivatedException.printStackTrace();
				}
			}
		});

		fahrenheit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					((Decorator_Fahrenheit) sensor).convert(sensor.getValue());
				} catch (SensorNotActivatedException e1) {
					e1.printStackTrace();
				}
			}
		});

		round.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					((Decorator_Arrondis) sensor).round(sensor.getValue());
				} catch (SensorNotActivatedException e1) {
					e1.printStackTrace();
				}
			}
		});

		vide.setEnabled(false);

		if (!actif) {
			fahrenheit.setEnabled(false);
			round.setEnabled(false);
		}

		if (arrondis) {
			fahrenheit.setEnabled(false);
		} else
			round.setEnabled(false);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new GridLayout(2, 3));
		buttonsPanel.add(round);
		buttonsPanel.add(fahrenheit);
		buttonsPanel.add(vide);
		buttonsPanel.add(on);
		buttonsPanel.add(off);
		buttonsPanel.add(update);

		this.add(buttonsPanel, BorderLayout.SOUTH);
	}

	public void update(Observable o, Object arg) {
		try {
			value.setText("" + sensor.getValue());
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}
	}
}
